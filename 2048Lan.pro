#-------------------------------------------------
#
# Project created by QtCreator 2016-05-08T13:52:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2048Lan
TEMPLATE = app


SOURCES += main.cpp\
    Core/Observer.cpp \
    Core/Observable.cpp \
    Core/Tile.cpp \
    Core/Board.cpp \
    UI/QGameOverWindow.cpp \
    UI/QGameWidget.cpp \
    UI/QTileLabel.cpp \
    Core/Game.cpp \

HEADERS  += \
    Core/Observer.h \
    Core/Observable.h \
    Core/Tile.h \
    Core/Board.h \
    UI/QGameOverWindow.h \
    UI/QGameWidget.h \
    UI/QTileLabel.h \
    Core/Game.h \
    Core/MovingDirection.h

CONFIG += c++11
