#include "Observable.h"
#include "Observer.h"

Observable::Observable()
{

}

void Observable::notifyObservers() const
{
    for(std::unordered_set<Observer *>::const_iterator iter = m_obs.begin(); iter != m_obs.end(); ++iter)
    {
        (*iter)->notify();
    }
}

void Observable::registerObs(Observer *obs)
{
    m_obs.insert(obs);
}

void Observable::removeObs(Observer *obs)
{
    m_obs.erase(obs);
}

