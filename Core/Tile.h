#ifndef TILE_H
#define TILE_H


class Tile
{
public:
    Tile();
    Tile(int value, bool updated);

    int value() const;
    void setValue(int value);

    bool isUpdated() const;
    void setIsUpdated(bool isUpdated);

private:
    int m_value;
    bool m_isUpdated;
};

#endif // TILE_H
