#include "Board.h"
#include "Core/Tile.h"
#include <stdexcept>
#include <stdlib.h>
#include <utility>

Board::Board(unsigned int boardDim) : m_boardDim(boardDim), m_isMovedLastRound(false), m_pointScoredLastRound(0), m_currLargestTileValue(2)
{
    resizeBoard(boardDim);
    initializeBoard(boardDim);
}

Board::Board(const Board &oneBoard)
{
    m_boardDim = oneBoard.boardDim();
    resizeBoard(m_boardDim);
    m_isMovedLastRound = oneBoard.isMovedLastRound();
    m_pointScoredLastRound = oneBoard.pointScoredLastRound();
    m_currLargestTileValue = oneBoard.currLargestTileValue();
    // do deep copy
    for(unsigned int j = 0; j < m_boardDim; ++j)
    {
        for(unsigned int i = 0; i < m_boardDim; ++i)
        {
            m_tiles[i][j] = std::shared_ptr<Tile>(new Tile(oneBoard.tiles()[i][j]->value(), oneBoard.tiles()[i][j]->isUpdated()));
        }
    }
}

void Board::resizeBoard(unsigned int boardDim)
{
    // set size
    std::shared_ptr<Tile> oneTile = std::shared_ptr<Tile>(new Tile(0, false));
    std::vector<std::shared_ptr<Tile>> oneRow;
    oneRow.resize(boardDim, oneTile);
    m_tiles.resize(boardDim, oneRow); // at this point, all the shared pointers refer to the same object, shallow copy
}

void Board::move(Direction direction)
{
    prepareForNextMove();

    switch(direction)
    {
    case Direction::UP:
        for(unsigned int i = 0; i < m_boardDim; ++i)
        {
            for(unsigned int j = 0; j < m_boardDim; ++j)
            {
                moveOneTile(i, j, direction);
            }
        }

        break;
    case Direction::DOWN:
        for(int i = m_boardDim - 1; i >= 0; --i)
        {
            for(unsigned int j = 0; j < m_boardDim; ++j)
            {
                moveOneTile(i, j, direction);
            }
        }

        break;
    case Direction::LEFT:
        for(unsigned int j = 0; j < m_boardDim; ++j)
        {
            for(unsigned int i = 0; i < m_boardDim; ++i)
            {
                moveOneTile(i, j, direction);
            }
        }

        break;
    case Direction::RIGHT:
        for(int j = m_boardDim - 1; j >= 0; --j)
        {
            for(unsigned int i = 0; i < m_boardDim; ++i)
            {
                moveOneTile(i, j, direction);
            }
        }

        break;
    default:
        throw std::logic_error("Invalid move direction");
    }

    if (m_isMovedLastRound)
    {
        newTile();
    }
}

std::vector<std::vector<std::shared_ptr<Tile> > > Board::tiles() const
{
    return m_tiles;
}

unsigned int Board::boardDim() const
{
    return m_boardDim;
}

int Board::pointScoredLastRound() const
{
    return m_pointScoredLastRound;
}

bool Board::isMovedLastRound() const
{
    return m_isMovedLastRound;
}


void Board::moveOneTile(unsigned int i, unsigned int j, Direction direction)
{
    if (m_tiles[i][j]->value() == 0) return; // do not move empty tile

    if (direction == Direction::LEFT)
    {
        unsigned int posNearTile = j;
        while(posNearTile > 0 && m_tiles[i][posNearTile - 1]->value() == 0)
        {
            moveTileToEmptySpot(m_tiles[i][posNearTile].get(), m_tiles[i][posNearTile - 1].get());
            posNearTile--;
        }
        // means we do not reach the border and its neighbor in the north is not null, check wether we can merge them
        if (posNearTile != 0 && isOkToMerge(m_tiles[i][posNearTile].get(), m_tiles[i][posNearTile - 1].get()))
        {
            mergeFirstTileToSecond(m_tiles[i][posNearTile].get(), m_tiles[i][posNearTile - 1].get());
        }
    }
    else if (direction == Direction::RIGHT)
    {
        unsigned int posNearTile = j;
        while(posNearTile < m_boardDim - 1 && m_tiles[i][posNearTile + 1]->value() == 0)
        {
            moveTileToEmptySpot(m_tiles[i][posNearTile].get(), m_tiles[i][posNearTile + 1].get());
            posNearTile++;
        }

        if (posNearTile != m_boardDim - 1 && isOkToMerge(m_tiles[i][posNearTile].get(), m_tiles[i][posNearTile + 1].get()))
        {
            mergeFirstTileToSecond(m_tiles[i][posNearTile].get(), m_tiles[i][posNearTile + 1].get());
        }
    }
    else if (direction == Direction::DOWN)
    {
        unsigned int posNearTile = i;
        while(posNearTile < m_boardDim - 1 && m_tiles[posNearTile + 1][j]->value() == 0)
        {
            moveTileToEmptySpot(m_tiles[posNearTile][j].get(), m_tiles[posNearTile + 1][j].get());
            posNearTile++;
        }

        if (posNearTile != m_boardDim - 1 && isOkToMerge(m_tiles[posNearTile][j].get(), m_tiles[posNearTile + 1][j].get()))
        {
            mergeFirstTileToSecond(m_tiles[posNearTile][j].get(), m_tiles[posNearTile + 1][j].get());
        }
    }
    else if (direction == Direction::UP)
    {
        unsigned int posNearTile = i;
        while(posNearTile > 0 && m_tiles[posNearTile - 1][j]->value() == 0)
        {
            moveTileToEmptySpot(m_tiles[posNearTile][j].get(), m_tiles[posNearTile - 1][j].get());
            posNearTile--;
        }

        if (posNearTile != 0 && isOkToMerge(m_tiles[posNearTile][j].get(), m_tiles[posNearTile - 1][j].get()))
        {
            mergeFirstTileToSecond(m_tiles[posNearTile][j].get(), m_tiles[posNearTile - 1][j].get());
        }
    }
    else
    {
        throw std::logic_error("Invalid move direction");
    }
}

bool Board::isOkToMerge(const Tile *curr, const Tile *neighbor) const
{
    if (neighbor->isUpdated() == false && curr->isUpdated() == false && neighbor->value() == curr->value())
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Board::mergeFirstTileToSecond(Tile *first, Tile *second)
{
    second->setValue(second->value() * 2);
    second->setIsUpdated(true);
    first->setValue(0);
    first->setIsUpdated(false);
    m_isMovedLastRound = true;
    m_pointScoredLastRound += second->value();

    // update current largest value of tile
    if (second->value() > m_currLargestTileValue)
    {
        m_currLargestTileValue = second->value();
    }
}

void Board::moveTileToEmptySpot(Tile *tile, Tile *emptySpot)
{
    emptySpot->setValue(tile->value());
    emptySpot->setIsUpdated(false);
    tile->setValue(0);
    tile->setIsUpdated(false);
    m_isMovedLastRound = true;
}

void Board::initializeBoard(unsigned int boardDim)
{
    for(unsigned int j = 0; j < boardDim; ++j)
    {
        for(unsigned int i = 0; i < boardDim; ++i)
        {
            m_tiles[i][j] = std::shared_ptr<Tile>(new Tile(0, false));
        }
    }

    // generat two tiles with value 2 in the beginning
    for(int k = 0; k < 2; ++k)
    {
        unsigned int i = rand() % m_boardDim, j = rand() % m_boardDim;
        m_tiles[i][j] = std::shared_ptr<Tile>(new Tile(2, false));
    }
}

void Board::newTile()
{
    std::vector<std::pair<int, int> > emptyTiles = findEmptyTiles();
    if (!emptyTiles.empty())
    {
        int index = rand() % emptyTiles.size();
        if (rand() % 2 == 0)
        {
            m_tiles[emptyTiles[index].first][emptyTiles[index].second] = std::shared_ptr<Tile>(new Tile(2, false));
        }
        else
        {
            m_tiles[emptyTiles[index].first][emptyTiles[index].second] = std::shared_ptr<Tile>(new Tile(4, false));
        }
    }
}

void Board::prepareForNextMove()
{
    for(unsigned int j = 0; j < m_boardDim; ++j)
    {
        for(unsigned int i = 0; i < m_boardDim; ++i)
        {
            m_tiles[i][j]->setIsUpdated(false);
        }
    }

    m_isMovedLastRound = false;
    m_pointScoredLastRound = 0;
}

std::vector<std::pair<int, int> > Board::findEmptyTiles() const
{
    std::vector<std::pair<int, int>> emptyTiles;
    for(unsigned int i = 0; i < m_boardDim; ++i)
    {
        for(unsigned int j = 0; j < m_boardDim; ++j)
        {
            if(m_tiles[i][j]->value() == 0)
            {
                emptyTiles.push_back(std::make_pair(i, j));
            }
        }
    }

    return emptyTiles;
}

bool Board::isMovePossible() const
{
    if (findEmptyTiles().empty())
    {
        // check if there is still a move to do
        Board newBoard(*this); // this is deep copy since we implement deep copy
        newBoard.move(UP);
        if (newBoard.isMovedLastRound()) return true;
        newBoard.move(DOWN);
        if (newBoard.isMovedLastRound()) return true;
        newBoard.move(LEFT);
        if (newBoard.isMovedLastRound()) return true;
        newBoard.move(RIGHT);
        if (newBoard.isMovedLastRound()) return true;

        // no possible move
        return false;
    }
    else
    {
        return true;
    }
}

void Board::reset()
{
    m_isMovedLastRound = false;
    m_pointScoredLastRound = 0;
    m_currLargestTileValue = 2;
    initializeBoard(m_boardDim);
}

int Board::currLargestTileValue() const
{
    return m_currLargestTileValue;
}

