#ifndef MOVINGDIRECTION_H
#define MOVINGDIRECTION_H


enum Direction
{
    UP,
    DOWN,
    RIGHT,
    LEFT
};

#endif // MOVINGDIRECTION_H
