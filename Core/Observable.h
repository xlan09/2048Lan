#ifndef OBSERVABLE_H
#define OBSERVABLE_H
#include <unordered_set>

class Observer;

class Observable
{
public:
    Observable();
    void registerObs(Observer *obs);
    void removeObs(Observer *obs);

protected:
    void notifyObservers() const;

private:
    std::unordered_set<Observer *> m_obs;
};

#endif // OBSERVABLE_H
