#include "Tile.h"

Tile::Tile() : m_value(0), m_isUpdated(false)
{

}

Tile::Tile(int value, bool updated) : m_value(value), m_isUpdated(updated)
{

}

int Tile::value() const
{
    return m_value;
}

void Tile::setValue(int value)
{
    m_value = value;
}

bool Tile::isUpdated() const
{
    return m_isUpdated;
}

void Tile::setIsUpdated(bool isUpdated)
{
    m_isUpdated = isUpdated;
}



