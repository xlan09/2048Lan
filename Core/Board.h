#ifndef BOARD_H
#define BOARD_H
#include <vector>
#include <memory>
#include "Core/MovingDirection.h"

class Tile;

class Board
{
public:
    Board(unsigned int boardDim);
    Board(const Board &oneBoard);
    void move(Direction direction);
    std::vector<std::vector<std::shared_ptr<Tile> > > tiles() const;
    unsigned int boardDim() const;
    int pointScoredLastRound() const;
    bool isMovedLastRound() const;
    bool isMovePossible() const;
    void reset();
    int currLargestTileValue() const;

private:
    unsigned int m_boardDim;
    bool m_isMovedLastRound;
    int m_pointScoredLastRound;
    int m_currLargestTileValue;
    std::vector<std::vector<std::shared_ptr<Tile>>> m_tiles;

    void moveOneTile(unsigned int i, unsigned int j, Direction direction);
    bool isOkToMerge(const Tile *curr, const Tile *neighbor) const;
    void mergeFirstTileToSecond(Tile *first, Tile *second);
    void moveTileToEmptySpot(Tile *tile, Tile *emptySpot);
    void initializeBoard(unsigned int boardDim);
    void newTile();
    void prepareForNextMove();
    std::vector<std::pair<int, int>> findEmptyTiles() const;
    void resizeBoard(unsigned int boardDim);
};

#endif // BOARD_H
