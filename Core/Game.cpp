#include "Game.h"
#include "Core/Board.h"

const int Game::ms_WinningScore = 2048;

Game::Game(unsigned int boardDim) : m_score(0), m_isGameOver(false)
{
    m_board = std::unique_ptr<Board>(new Board(boardDim));
}

Board *Game::board() const
{
    return m_board.get();
}

int Game::score() const
{
    return m_score;
}

bool Game::isGameOver() const
{
    return m_isGameOver;
}

void Game::move(Direction direction)
{
    m_board->move(direction);
    m_score += m_board->pointScoredLastRound();
    if(m_board->isMovePossible() == false)
    {
        m_isGameOver = true;
    }

    notifyObservers();
}

void Game::restart()
{
    m_board->reset();
    m_score = 0;
    m_isGameOver = false;
}

bool Game::won() const
{
   return ms_WinningScore == m_board->currLargestTileValue();
}

int Game::winningScore()
{
    return ms_WinningScore;
}

