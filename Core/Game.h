#ifndef GAME_H
#define GAME_H

#include "Core/Observable.h"
#include "Core/MovingDirection.h"
#include <memory>
class Board;

/**
 * @brief The GameMonitor class: used for communication between UI and board
 */
class Game : public Observable
{
public:
    Game(unsigned int boardDim);
    void move(Direction direction);
    Board *board() const;
    int score() const;
    bool isGameOver() const;
    void restart();
    bool won() const;
    static int winningScore();

private:
    int m_score;
    bool m_isGameOver;
    std::unique_ptr<Board> m_board;
    static const int ms_WinningScore;
};

#endif // GAME_H
