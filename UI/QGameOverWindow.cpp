#include "QGameOverWindow.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>

QGameOverWindow::QGameOverWindow(QWidget *parent) : QWidget(parent)
{
    setStyleSheet("QGameOverWindow { background: rgb(237,224,200); }");
    setFixedSize(425,205);
    QVBoxLayout *layout = new QVBoxLayout(this);
    // game over label
    QLabel* gameover = new QLabel("Game Over!", this);
    gameover->setStyleSheet("QLabel { color: rgb(119,110,101); font: 40pt; font: bold;} ");
    // reset button
    m_reset = new QPushButton("Retry", this);
    m_reset->setFixedHeight(50);
    m_reset->setFixedWidth(100);
    // add game over label to window
    layout->insertWidget(0,gameover,0,Qt::AlignCenter);
    // add reset button to window
    layout->insertWidget(1,m_reset,0,Qt::AlignCenter);
}

QPushButton *QGameOverWindow::reset() const
{
    return m_reset;
}
