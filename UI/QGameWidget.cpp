#include "QGameWidget.h"
#include "Core/Game.h"
#include "Core/Board.h"
#include "Core/Tile.h"
#include "UI/QTileLabel.h"
#include <QVBoxLayout>
#include <QGridLayout>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QPushButton>

QGameWidget::QGameWidget(unsigned int boardDim, QWidget *parent) : QWidget(parent)
{
    setFixedSize(650, 450);
    m_centralLayout = new QVBoxLayout();
    setLayout(m_centralLayout);
    // style sheet of the board
    setStyleSheet("QGameWidget { background-color: rgb(187,173,160) }");

    // game
    m_game = std::unique_ptr<Game>(new Game(boardDim));
    m_game->registerObs(this);

    m_score = new QLabel(QString("SCORE: %1").arg(m_game->score()));
    m_score->setStyleSheet("QLabel { color: rgb(70,3, 3); font: 16pt; }");
    m_score->setFixedHeight(50);
    m_centralLayout->insertWidget(1, m_score, 0, Qt::AlignRight);

    m_boardLayout = nullptr;

    std::vector<std::shared_ptr<QTileLabel>> oneRow;
    oneRow.resize(boardDim, nullptr);
    m_QTileLabels.resize(boardDim, oneRow);

    connect(m_gameOverWindow.reset(), SIGNAL(clicked()), this, SLOT(resetGame()));

    plotBoard();
}

QGameWidget::~QGameWidget()
{

}

void QGameWidget::notify()
{
    if (m_game->isGameOver())
    {
        m_gameOverWindow.show();
    }

    if (m_game->won())
    {
        m_score->setText(QString("You hit %1, congratulations! SCORE: %2").arg(Game::winningScore()).arg(m_game->score()));
    }
    else
    {
        // update score
        m_score->setText(QString("SCORE: %1").arg(m_game->score()));
    }

    plotBoard();
}

void QGameWidget::resetGame()
{
    m_game->restart();
    plotBoard();
    m_score->setText(QString("SCORE: %1").arg(m_game->score()));
    m_gameOverWindow.hide();
}

void QGameWidget::plotBoard()
{
    delete m_boardLayout;
    m_boardLayout = new QGridLayout(); // delete old layout and create a new layout
    std::vector<std::vector<std::shared_ptr<Tile> > > tiles = m_game->board()->tiles();
    //TODO: better to use iterator to loop through vectors
    for(unsigned int i = 0; i < tiles.size(); ++i)
    {
        for(unsigned int j = 0; j < tiles[0].size(); ++j)
        {
            m_QTileLabels[i][j] = std::shared_ptr<QTileLabel>(new QTileLabel(tiles[i][j].get()));
            m_boardLayout->addWidget(m_QTileLabels[i][j].get(), i, j);
            m_QTileLabels[i][j]->draw();
        }
    }

    m_centralLayout->insertLayout(0, m_boardLayout);
}

void QGameWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Up:
        m_game->move(UP);
        break;
    case Qt::Key_Left:
        m_game->move(LEFT);
        break;
    case Qt::Key_Right:
        m_game->move(RIGHT);
        break;
    case Qt::Key_Down:
        m_game->move(DOWN);
        break;
    }
}
