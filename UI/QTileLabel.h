#ifndef QTILELABEL_H
#define QTILELABEL_H
#include <QLabel>
class Tile;

class QTileLabel : public QLabel
{
    Q_OBJECT
public:
    QTileLabel(const Tile *tile);
    void draw();

private:
    const Tile *m_tilePtr;
};

#endif // QTILELABEL_H
