#ifndef QGAMEOVERWINDOW_H
#define QGAMEOVERWINDOW_H

#include <QWidget>

class QPushButton;

class QGameOverWindow : public QWidget
{
    Q_OBJECT
public:
    explicit QGameOverWindow(QWidget *parent = 0);

    QPushButton *reset() const;

private:
    QPushButton* m_reset;
};

#endif // QGAMEOVERWINDOW_H
