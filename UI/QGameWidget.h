#ifndef QGAMEWIDGET_H
#define QGAMEWIDGET_H

#include <QWidget>
#include <vector>
#include "Core/Observer.h"
#include "UI/QGameOverWindow.h"
#include <memory>

class QLabel;
class QTileLabel;
class QVBoxLayout;
class QGridLayout;
class Game;

class QGameWidget : public QWidget, public Observer
{
    Q_OBJECT
public:
    explicit QGameWidget(unsigned int boardDim, QWidget *parent = 0);
    ~QGameWidget();
    void notify();

public slots:
    void resetGame();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    QVBoxLayout *m_centralLayout;
    std::unique_ptr<Game> m_game;
    QLabel *m_score;
    QGridLayout *m_boardLayout;
    std::vector<std::vector<std::shared_ptr<QTileLabel>>> m_QTileLabels;
    QGameOverWindow m_gameOverWindow;
//    QLabel *m_winSign;

    void plotBoard();
};

#endif // QGAMEWIDGET_H
