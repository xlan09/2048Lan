#include <QApplication>
#include "UI/QGameWidget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QGameWidget game(4);
    game.show();

    return a.exec();
}
